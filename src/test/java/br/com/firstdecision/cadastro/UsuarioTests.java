package br.com.firstdecision.cadastro;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import br.com.firstdecision.cadastro.controller.UsuarioController;
import br.com.firstdecision.cadastro.model.Usuario;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class UsuarioTests {
	
	private Usuario usuario;
	private Long id;
	
//	@Autowired
//	private WebTestClient testClient;
	
	@BeforeEach
	void setUp() {
		
		id = 1L;
		usuario = mock( Usuario.class );
		when( usuario.getId() ).thenReturn( id );
		when( usuario.getNome() ).thenReturn( "Fernando Oliveira Silva" );
		when( usuario.getEmail() ).thenReturn( "fernando.silva10@gmail.com" );
		when( usuario.getSenha() ).thenReturn( "agh77852" );
		
	}
	
	@Nested
	class Salvar {
		
		@Test
		@Order(1)
		void testSalvar() {
			
			WebTestClient testClient = WebTestClient.bindToController(UsuarioController.class).build();
			
			testClient.post().uri( "/api/v1/usuarios" )
			.accept( MediaType.APPLICATION_JSON )
			.bodyValue( usuario )
			.exchange()
				.expectStatus().isCreated();
			
		}
		
	}

}

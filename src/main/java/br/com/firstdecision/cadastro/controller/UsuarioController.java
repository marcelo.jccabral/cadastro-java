package br.com.firstdecision.cadastro.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.firstdecision.cadastro.exception.ResourceNotFoundException;
import br.com.firstdecision.cadastro.exception.ValidacaoUsuarioException;
import br.com.firstdecision.cadastro.model.Usuario;
import br.com.firstdecision.cadastro.service.UsuarioService;


@RestController
@RequestMapping("/api/v1")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	//pegar todas as contas
	@GetMapping("/usuarios")
	@ResponseStatus(HttpStatus.OK)
	public List<Usuario> getAllUsuarios(){
		
		return this.usuarioService.getAll();
		
	}
	
	//pegar a conta pelo id
	@GetMapping("/usuarios/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Usuario> getById(@PathVariable(value = "id") Long usuarioId)
	    throws ResourceNotFoundException {
		
		Usuario usuario = usuarioService.getById(usuarioId);
	    return ResponseEntity.ok().body( usuario );
	    
	}
	
	//salvar conta
	@PostMapping("/usuarios")
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario create(@RequestBody Usuario usuario) throws ValidacaoUsuarioException {
		
		return this.usuarioService.create(usuario);
		
	}
	
	//atualizar conta
	@PutMapping("/usuarios/{id}")
	@ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Usuario> update(@PathVariable(value = "id") Long usuarioId,
    	@Validated @RequestBody Usuario usuarioAlteracao) throws ResourceNotFoundException, ValidacaoUsuarioException {
		
		Usuario usuario = usuarioService.getById(usuarioId);
        
		usuario.setNome(usuarioAlteracao.getNome());
		usuario.setEmail(usuarioAlteracao.getEmail());
		usuario.setSenha(usuarioAlteracao.getSenha());
        
        return ResponseEntity.ok(this.usuarioService.create(usuario));
        
    }
	
	//deletar conta
	@DeleteMapping("/usuarios/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long usuarioId) 
			throws ResourceNotFoundException {
	    
		Usuario usuario = usuarioService.getById(usuarioId);
		
	    this.usuarioService.delete(usuario.getId());
	    
	    Map<String, Boolean> resposta = new HashMap<>();
	    resposta.put("Usuário deletado", Boolean.TRUE);
	    return resposta;
	    
	}

}
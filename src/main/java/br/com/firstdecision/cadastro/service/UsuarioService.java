package br.com.firstdecision.cadastro.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.firstdecision.cadastro.exception.ResourceNotFoundException;
import br.com.firstdecision.cadastro.exception.TipoErroValidacao;
import br.com.firstdecision.cadastro.exception.ValidacaoUsuarioException;
import br.com.firstdecision.cadastro.model.Usuario;
import br.com.firstdecision.cadastro.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public List<Usuario> getAll(){		
		
		return this.usuarioRepository.findAll();
		
	}
	
	public Usuario getById(Long usuarioId)
	    throws ResourceNotFoundException {
		
		Usuario usuario = usuarioRepository.findById(usuarioId)
	      .orElseThrow(() -> new ResourceNotFoundException("Usuário não encontrado para o ID :: " + usuarioId));
	    return usuario;
	    
	}
	
	public Usuario create(Usuario usuario) throws ValidacaoUsuarioException {
		
		if(usuarioRepository.findByEmail(usuario.getEmail()).isPresent()) {
			throw new ValidacaoUsuarioException("Usuário existente para o email informado.", TipoErroValidacao.USUARIO_EXISTENTE);
		}
		
		if(!usuario.getSenha().equals(usuario.getConfirmacaoSenha())) {
			throw new ValidacaoUsuarioException("A Senha e a Confirmação da Senha estão diferentes.", TipoErroValidacao.VALIDACAO_CAMPOS);
		}
		
		return this.usuarioRepository.save(usuario);
		
	}
	
	public Usuario update(Long usuarioId, Usuario usuarioAlteracao) throws ResourceNotFoundException {
		Usuario usuario = usuarioRepository.findById(usuarioId)
        .orElseThrow(() -> new ResourceNotFoundException("Usuário não encontrado para o ID :: " + usuarioId));
        
		usuario.setNome(usuarioAlteracao.getNome());
		usuario.setEmail(usuarioAlteracao.getEmail());
		usuario.setSenha(usuarioAlteracao.getSenha());
        
        return this.usuarioRepository.save(usuario);
        
    }
	
	public Map<String, Boolean> delete(Long usuarioId) 
			throws ResourceNotFoundException {
		
	    Usuario usuario = usuarioRepository.findById(usuarioId)
	   .orElseThrow(() -> new ResourceNotFoundException("Usuário não encontrado para o ID :: " + usuarioId));
	
	    this.usuarioRepository.delete(usuario);
	    Map<String, Boolean> resposta = new HashMap<>();
	    resposta.put("Usuário deletado", Boolean.TRUE);
	    
	    return resposta;
	    
	}
	
}

package br.com.firstdecision.cadastro.exception;

import java.util.Date;

public class ErrorDetails {
	
	private Integer codigo;
	private Date data;
	private String mensagem;
	private String detalhes;
	
	public ErrorDetails(Integer codigo, Date data, String mensagem, String detalhes) {
		super();
		this.codigo = codigo;
		this.data = data;
		this.mensagem = mensagem;
		this.detalhes = detalhes;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getDetalhes() {
		return detalhes;
	}
	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}
		
}
package br.com.firstdecision.cadastro.exception;

public enum TipoErroValidacao {

	VALIDACAO_CAMPOS(1),
	USUARIO_EXISTENTE(2);
	
	private Integer codigo;

	private TipoErroValidacao(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
}

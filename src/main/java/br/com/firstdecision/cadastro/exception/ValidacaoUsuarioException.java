package br.com.firstdecision.cadastro.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ValidacaoUsuarioException extends Exception{

    private static final long serialVersionUID = 1L;

    private TipoErroValidacao tipoErro;
    
    public ValidacaoUsuarioException(String message){
        super(message);
    }
    
    public ValidacaoUsuarioException(String message, TipoErroValidacao tipoErro){
        super(message);
        this.tipoErro = tipoErro;
    }

	public TipoErroValidacao getTipoErro() {
		return tipoErro;
	}

	public void setTipoErro(TipoErroValidacao tipoErro) {
		this.tipoErro = tipoErro;
	}
	
}